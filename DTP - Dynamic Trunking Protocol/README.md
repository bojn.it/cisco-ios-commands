# DTP - Dynamic Trunking Protocol

## Mind Map

```mermaid
graph LR
    DTP --- Modes
    DTP --- Communication
    DTP --- Prerequisites
    DTP --- RelatedCommands["Related Commands"]

    Modes --- Trunk
    Modes --- DynamicDesirable["Dynamic Desirable"]
    Modes --- DynamicAuto["Dynamic Auto"]
    Modes --- Access
    Communication --- Advertisements
    RelatedCommands --- Verification
```

## DTP

-   Dynamic Trunking Protocol.

## Trunk `(in Modes)`

-   Manually sets a port as a trunk.
-   The other side cannot be in Access mode only and the trunk will be created.

## Dynamic Desirable `(in Modes)`

-   A port acts as an access port.
-   Listens and sends DTP proposals to create trunk.

## Dynamic Auto `(in Modes)`

-   A port acts as an access port.
-   Listens only DTP proposals to create trunk.

## Access `(in Modes)`

-   A port acts as an access port only.
-   Cannot be a trunk ever.

## Advertisements `(in Communication)`

-   Sent every 30 seconds.
-   Switches exchange DTP packets and try to negotiate a trunk port between them.
-   They are sent on VLAN 1 if an encapsulation is ISL.
-   They are sent on Native VLAN if an encapsulation is 802.1Q.

## Prerequisites

-   Both the switches must have the same VTP domain set.

## Related Commands

```
SW(config-if)# switchport trunk encapsulation {dot1q | isl | negotiate}
```

Sets the encapsulation.

```
SW(config-if)# switchport mode {trunk | dynamic {desirable | auto} | access}
```

Sets the mode.

```
SW(config-if)# switchport nonegotiate
```

Deactivates sending of advertisements.  
Use always when the Trunk mode is set (best practise).

## Verification `(in Related Commands)`

```
SW# show interfaces [<INTERFACE>] {trunk | switchport}
```

-   `<INTERFACE>`
    
    Limits the output to the given interface.

-   `trunk`
    
    Shows for the all existing trunk ports info such as:
    -   Port,
    -   Mode,
    -   Encapsulation,
    -   Status (e.g. trunking),
    -   Native VLAN (e.g. 1),
    -   Allowed VLANs (e.g. 1-4094).

-   `switchport`
    
    Shows info such as:
    -   Administrative Trunking Encapsulation,
    -   Operational Trunking Encapsulation,
    -   Negotiation of Trunking (e.g. Off),
    -   Trunking Native Mode VLAN (e.g. 1 (default)),
    -   Trunking VLANs Enabled (e.g. ALL).
