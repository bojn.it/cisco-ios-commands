# Cisco IOS Commands

## Content

- [Legend](#Legend)
- [VTP - VLAN Trunking Protocol](#VTP)
    - [vtp](#VTP--vtp)
- [EtherChannel](#EtherChannel)

--------------------------------------------------------------------------------

## Legend

```
<VAR>           Placeholder 
```

--------------------------------------------------------------------------------

## VTP - VLAN Trunking Protocol

### `vtp`

#### Command

```mermaid
graph LR
    $>"SW(config)#"] --> vtp
    vtp --> version
    version --> 1{{"1"}}
    version --> 2{{"2"}}
    version --> 3{{"3"}}
    vtp --> domain
    domain --> $domain{{"<DOMAIN>"}}
    vtp --> password
    password --> $password{{"<PASSWORD>"}}
    vtp --> primary
    vtp --> mode
    mode --> server{{"server"}}
    mode --> client{{"client"}}
    mode --> transparent{{"transparent"}}
    mode --> none{{"none"}}
```

- **version**
    > Sets the version.
    - 1
        > VLAN from 1 to 1005.
    - 2
        > VLAN from 1 to 1005.
    - 3
        > VLAN from 1 to 4094.

- **domain**
    > Sets the domain. Resets the local DB to revision 0.
    - <*DOMAIN*>

- **password**
    > Sets the password.
    > Prevents unauthorized switches from joining the VTP domain.
    - <*PASSWORD*>

- **primary**
    > Sets the VTP server as primary.

- **mode**
    > Sets the mode.
    - server
    - client
    - transparent
    - none

#### Verification

```
SW# show vtp status
```

- Shows the version, domain, operating mode, number of VLANs,
  configuration revision.

--------------------------------------------------------------------------------

## EtherChannel

### `channel-group`

#### Pre-conditions

```
SW(config)# interface range <RANGE>
```

#### Command

```mermaid
graph LR
    $>"SW(config-if-range)#"] --> channel-group
    channel-group --> $group-id{{"<GROUP-ID>"}}
    $group-id --> mode
    mode --> on{{"on"}}
    mode --> auto{{"auto"}}
    mode --> desirable{{"desirable"}}
    mode --> active{{"active"}}
    mode --> passive{{"passive"}}
    auto -.-> non-silent((non-silent))
    desirable -.-> non-silent
```

- **channel-group**
    > Determines the EtherChannel we want to work with.
    - <*GROUP-ID*>

- **mode**
    > Sets the mode.
    - on
        > Statically configured EtherChannel.
    - auto
        > PAgP.
    -  desirable
        > PAgP.
    - active
        > LACP.
    - passive
        > LACP.

- **non-silent**
    > By default, PAgP ports operate in silent mode which allows ports 
    > to establish an EtherChannel with a device that is not PAgP capable.
    > This option should be used with the PAgP capable switches.
    > A local port requires PAgP packet from the other side before
    > an EtherChannel will be established.

#### Post-conditions

```
SW(config)# interface port-channel <GROUP-ID>
SW(config-if)# switchport mode trunk
```

#### Verification

```
SW# show etherchannel summary
```

- Shows an overview of all configued EtherChannels along with status
  and protocol type.
    - An EtherChannel status should be SU which means L2 and In Use.

```
SW# show interfaces port-channel <GROUP-ID>
```

- Shows logical interface (EtherChannel itself).
- Shows statistics and member interfaces.
- Shows combined bandwidth throughput of all active member interfaces.
    - If the bandwidth is changed the combined bandwidth is adjusted.



### `port-channel`

#### Pre-conditions

```
SW(config)# interface range <RANGE>
SW(config-if-range)# channel-group <GROUP-ID> mode active|passive
SW(config-if-range)# exit
SW(config)# interface port-channel <GROUP-ID>
```

#### Command

```mermaid
graph LR
    $>"SW(config-if)#"] --> port-channel
    port-channel --> min-links
    min-links --> $number{{"<NUMBER>"}}
```

- **min-links**
    > LACP only? Sets the minimum number of active links
    > in order to EtherChannel could work properly.
    - <*NUMBER*>



### `lacp`

#### Pre-conditions

```
SW(config)# interface range <RANGE>
SW(config-if-range)# channel-group <GROUP-ID> mode active|passive
SW(config-if-range)# exit
SW(config)# interface port-channel <GROUP-ID>
```

#### Command

```mermaid
graph LR
    $>"SW(config-if)#"] --> lacp
    lacp --> max-bundle
    max-bundle --> $number{{"<NUMBER>"}}
```

- **max-bundle**
    > LACP only. An EtherChannel can be configured with the maximum
    > number of active links.
    > The rest is standby.
    - <*NUMBER*>
