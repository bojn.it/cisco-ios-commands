# VTP - VLAN Trunking Protocol

## Mind Map

```mermaid
graph LR
    VTP --- Modes
    VTP --- Versions
    VTP --- Communication
    VTP --- VLAN
    VTP --- RelatedCommands["Related Commands"]

    Modes --- Server
    Modes --- Client
    Modes --- Transparent
    Modes --- Off
    Communication --- Advertisements
    RelatedCommands --- Verification

    Advertisements --- Summary
    Advertisements --- Subset
    Advertisements --- ClientRequest["Client Request"]
```

## VTP

-   VLAN Trunking Protocol.

## Server `(in Modes)`

-   There can be more servers.
-   A primary server has to be tagged as primary.
-   It's something like CRUD for VLANs in the VTP domain.

## Client `(in Modes)`

-   Cannot modify VLAN (database).

## Transparent `(in Modes)`

-   Receives and forwards the advertisements but does not process them.
-   Has own local database with VLANs.

## Versions

1.  Propagates VLAN from 1 to 1005. Default.
2.  Propagates VLAN from 1 to 1005.
3.  Propagates VLAN from 1 to 4094.

## Communication

-   Updates are advertised by using a multicast address ? across the trunk links.
-   All the switches in the VTP domain receive the advertisements
    regardless of the mode.

## Summary `(in Communication -> Advertisements)`

-   Every 300 seconds is sent this type of advertisement
    or when VLAN is added, removed or changed.

## Subset `(in Communication -> Advertisements)`

-   Sent when the configuration is changed on the server.
-   Sent to the switches in the Client mode.

## Client Request `(in Communication -> Advertisements)`

-   When a switch with mode of Client and lower revision joins a domain
    and receives Summary with higher revision so then the switch sends
    this request.

## VLAN

-   When a VLAN is removed then an access port is moved to VLAN 1.
-   **It's important when a switch joins a domain its revision has to be 0
    so it does not override VLAN database!**

## Related Commands

```
R/SW(config)# vtp version {1 | 2 | 3}
```

Sets the version.

```
R/SW(config)# vtp domain <DOMAIN>
```

Sets the domain.  
Resets the local database to revision 0.

```
R/SW(config)# vtp password <PASSWORD>
```

Sets the password.  
Prevents an unauthorized devices from joining the VTP domain.

```
R/SW(config)# vtp mode {server | client | transparent | none}
```

Sets the mode.

```
R/SW# vtp primary
```

Tags the VTP server as primary.  
Must be set when the version is 3 and the mode is Server.

## Verification `(in Related Commands)`

```
R/SW# show vtp status
```

Shows info such as:
-   VTP Version running,
-   VTP Domain Name,
-   VTP Operating Mode,
-   Number of existing VLANs,
-   Number of existing extended VLANs,
-   Configuration Revision.
