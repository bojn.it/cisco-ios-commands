# EtherChannel

## Mind Map

```mermaid
graph LR
    EtherChannel --- Protocols
    EtherChannel --- RelatedCommands["Related Commands"]

    Protocols --- PAgP
    Protocols --- LACP
    RelatedCommands --- Verification

    PAgP --- PAgPModes["Modes"]
    LACP --- LACPModes["Modes"]

    PAgPModes --- Auto
    PAgPModes --- Desirable
    LACPModes --- Passive
    LACPModes --- Active
```

## EtherChannel

-   Synonyms:
    -   Port Channel,
    -   Link Aggregation.
-   IEEE 802.3AD.
-   Logical link which is load-balanced between member interfaces.
-   Can be used on layers:
    -   L2 (access and trunk),
    -   L3 (routing).
-   If at least one member link stay active then the process of calculation
    of STP protocol or L3 routing will not be performed.
-   Preferred way of creating EtherChannel is dynamic instead of static
    as it provides Integrity Health Check to ensure end-to-end connectivity.

## PAgP `(in Protocols)`

-   Port Aggregation Protocol.
-   Cisco proprietary.
-   **Advertises PAgP messages to multicast MAC address 0100:0CCC:CCCC.**
-   **Protocol code 0x0104.**

## Auto `(in Protocols -> PAgP -> Modes)`

-   An interface does not initiate establishing of EtherChannel.
-   Does not transmit PAgP packets.
-   If the switch receives PAgP packet from the other side then
    this interface responds and establishes PAgP adajcency.
-   If both sides are in Auto mode then the adjacency is not established.

## Desirable `(in Protocols -> PAgP -> Modes)`

-   In this mode an interface sends PAgP packets and tries to establish 
    adjacency with the other side.
-   The other side has to be in this or Auto mode.

## LACP `(in Protocols)`

-   Link Aggregation Control Protocol.
-   Open standard.
-   **Advertises LACP messages to multicast MAC address 0180:C200:0002.**

## Passive `(in Protocols -> LACP -> Modes)`

-   An interface does not initiate establishing of EtherChannel.
-   Does not transmit LACP packets.
-   If the switch receives LACP packet from the other side then
    this interface responds and establishes LACP adajcency.
-   If both sides are in Passive mode then the adjacency is not established.

## Active `(in Protocols -> LACP -> Modes)`

-   In this mode an interface sends LACP packets and tries to establish 
    adjacency with the other side.
-   The other side has to be in this or Passive mode.

## Related Commands

```
R/SW(config)# interface range <RANGE>
```

```
R/SW(config-if-range)# channel-group <CHANNEL-GROUP> mode {on | {auto | desirable} [non-silent] | active | passive}
```

Sets the mode.

-   `<CHANNEL-GROUP>`
    
    Identifies the EtherChannel we want to work with.

-   `on`
    
    Statically configured EtherChannel.

-   `auto`
    
    PAgP.

-   `desirable`

    PAgP.
    
-   `active`
    
    LACP.

-   `passive`
    
    LACP.

-   `non-silent`
    
    By default, PAgP ports operate in silent mode which allows ports 
    to establish an EtherChannel with a device that is not PAgP capable.  
    This option should be used with the PAgP capable switches.  
    A local port requires PAgP packet from the other side before
    an EtherChannel will be established.

```
R/SW(config)# interface Port-channel <CHANNEL-GROUP>
```

```
R/SW(config-if)# switchport mode trunk
```

## Verification `(in Related Commands)`

```
R/SW# show etherchannel [<CHANNEL-GROUP>] {summary | port}
```

-   `<CHANNEL-GROUP>`
    
    Limits the output to the given EtherChannel.

-   `summary`

    Shows an overview of all configured EtherChannels along with:  
    -   Group (e.g. 1),
    -   Port-channel (e.g. Po1(*flags*)),
    -   Protocol (e.g. LACP),
    -   Ports (e.g. Gi0/1(*flags*), Gi0/2(*flags*)).

-   `port`

    Shows verbose local and remote/neighbor details of the configuration
    and information from the packets.  
    The output is grouped by EtherChannel IDs.  
    Shows details about every EtherChannel such as:
    -   Channel group (e.g. 1),
    -   Port-channel (e.g. Po1),
    -   Mode (e.g. Active),
    -   Protocol (e.g. LACP).
    
    For LACP, shows local info about every member interface such as:
    -   Port (e.g. Gi0/1),
    -   Flags (e.g. FA),
    -   State (e.g. bnd1),
    -   LACP port Priority (e.g. 32768).
    
    For LACP, shows neighbor info about every member interface such as:
    -   Port (e.g. Gi0/1),
    -   Flags (e.g. FA),
    -   Dev ID (MAC address),
    -   LACP port Priority (e.g. 32768),
    -   Age (e.g. 1s).

    For PAgP, shows local info about every member interface such as:
    -   Port (e.g. Gi0/1),
    -   Flags (e.g. SC),
    -   State (e.g. U6/S7),
    -   Timers (e.g. H),
    -   Hello Interval (e.g. 30s),
    -   Partner Count (e.g. 1),
    -   PAgP Priority (e.g. 32768).

    For PAgP, shows neighbor's info about every member interface such as:
    -   Port (e.g. Gi0/1),
    -   Partner Name (e.g. SW2),
    -   Partner Device ID (MAC address),
    -   Partner Port (twice, but why?),
    -   Age (e.g. 1s),
    -   Partner Flags (e.g. SC).
